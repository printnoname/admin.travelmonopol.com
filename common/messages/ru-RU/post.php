<?php
return [

    'ID' => 'Идентификатор',
    'Contents'=>'Содержание',
    'Alias' => 'Алиас',
    'Title'=>'Заголовок',
    'Status'=>'Статус публикации',
    'Category ID'=>'Идентификатор Категории',
    'Publish Date'=>'Дата публикации',
    'Created At'=>'Дата создания',
    'Deleted At'=>'Дата удаления',
    'Thumbnail'=>'Превью',
    'Image'=>'Изображение',
    'User ID'=>'Идентификатор Пользователя',
    'Image Author'=>'Автор изображения',
    'Image Description'=>'Описание изображения'
];