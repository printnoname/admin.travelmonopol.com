<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m180217_201823_fill_rbac
 */
class m180217_201823_fill_rbac extends Migration
{
<<<<<<< HEAD
    public function up()
    {
=======
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $user = new User();

        $user->username = "admin";
        $user->email = "admin@travelmonopol.com";
        $user->setPassword('upov00000');
        $user->generateAuthKey();
        $user->created_at = \Carbon\Carbon::now()->timestamp;
        $user->updated_at = \Carbon\Carbon::now()->timestamp;
        $user->save();

        $auth = \Yii::$app->authManager;

        $admin = $auth->createRole('Admin');
        $auth->add($admin);

        $superPermission = $auth->createPermission('superPermission');
        $superPermission->description = 'God Mode';
        $auth->add($superPermission);
>>>>>>> 67c116947ec3f82dbea3fa9589d119b1120d85ec

        $auth->addChild($admin,$superPermission);
        $auth->assign($admin, 1);
    }

    public function down()
    {

    }

}
