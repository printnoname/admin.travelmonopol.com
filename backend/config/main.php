<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'bootstrap' => ['log'],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'backend\controllers\CustomElFinderController',
            'plugin'=>[
                [
                    'class'=>'backend\utilities\ImageManager',
                    'enable'=>true
                ],
            ],
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl'=>'@staticUrl',
                    'basePath'=>'@static',
                    'path' => 'images',
                    'name' => 'images',
                    'plugin' => [
                        'ImageManager'=>[

                            'operation_flag'=>1
                        ]
                    ]
                ],
            ]
        ],
        'elfinderThumbnail'=>[
            'class'=>'backend\controllers\CustomElFinderController',
            'plugin' =>
                [
                    [
                        'class'=>'backend\utilities\ImageManager',
                        'enable'=>true
                    ]
                ],

            'access' => ['@'],
            'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
            'roots' => [
                [
                    'baseUrl'=>'@staticUrl',
                    'basePath'=>'@static',
                    'path' => 'thumbnails',
                    'name' => 'thumbnails',
                    'plugin' => [
                        'ImageManager' => [
                            'width'=>156,
                            'height'=>106,
                            'operation_flag'=>0
                        ]
                    ]
                ],
            ]
        ]
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=travelmonopol_db',
            'username' => 'root',
            'password' => 'neverlasting777!',
            'charset' => 'utf8',
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['Guest']
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/tours'=>'site/tours'
            ],
        ],
        'i18n' => [
            'translations' => [
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'backend/forms' => 'forms.php',
                        'backend/error' => 'error.php'
                    ],
                ],
            ],
        ],
        
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.*.*', 'XXX.XXX.XXX.XXX'] // adjust this to your needs
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'backend\controllers\RBACAssignmentController',
                ],
            ],
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Admin'],
                    ]
                ]
            ]

        ],
    ],
    'params' => $params,
];
