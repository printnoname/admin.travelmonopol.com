<?php
namespace backend\controllers;

use backend\models\SignupForm;
use backend\models\TourDate;
use backend\models\TourPreview;
use phpDocumentor\Reflection\Types\Nullable;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\TourSearch;
use backend\models\Tour;
use Carbon\Carbon;
use yii\helpers\Json;
use common\models\User;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','register'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['tours','create-tour','subregion','view','update','delete'],
                        'roles' => ['guidePermission'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionOrders(){

    }

    public function actionTours()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('tours-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id=NULL){
        $tour = Tour::find()->andWhere(['id'=>$id])->one();
        $tour_preview = TourPreview::find()->where(['id'=>$tour->tour_preview_id])->one();

        return $this->render('tour-view',[
            'tour'=>$tour,
            'tour_preview'=>$tour_preview
        ]);
    }

    public function actionUpdate($id=NULL)
    {


        $tour = Tour::find()->andWhere(['id' => $id])->one();
        $tourPreview = TourPreview::find()->where(['id' => $tour->tour_preview_id])->one();

        $tourDate = new TourDate();

        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        if ($tour->load(Yii::$app->request->post()) && $tourPreview->load(Yii::$app->request->post()) ){

            $transaction = \Yii::$app->db->beginTransaction();
            try {

                $tour->name = $tourPreview->name;

                if (!$tourPreview->validate()) {

                    $transaction->rollBack();
                    return $this->render('tour-edit', [
                        'modelDates'=> [$tourDate],
                        'tour' => $tour,
                        'tourPreview' => $tourPreview
                    ]);


                }

                $tourPreview->update();



                $dates = Yii::$app->request->post('TourDate');

                $dateObject = new \stdClass();

                foreach ($dates as $date) {

                    $dayofWeek = $date['day_of_week'];
                    if(isset($dateObject->$dayofWeek)) {
                        $dateObject->$dayofWeek[Carbon::createFromFormat('H:i',$date['time'])->format('G:i')] =
                            [
                                'price_child'=>intval($date['price_child']),
                                'price_adult'=>intval($date['price_adult'])
                            ];
                    }
                    else {
                        $dateObject->$dayofWeek =
                            [
                                (Carbon::createFromFormat('H:i',$date['time'])->format('G:i'))=>[
                                    'price_child'=>intval($date['price_child']),
                                    'price_adult'=>intval($date['price_adult'])
                                ],
                            ];
                    }

                }

                $tour->dates = json_encode($dateObject);

                if (!$tour->validate()) {
                    $transaction->rollBack();
                    return $this->render('tour-edit', [
                        'modelDates'=> [$tourDate],
                        'tour' => $tour,
                        'tourPreview' => $tourPreview
                    ]);
                }

                $tour->update();
            }
            catch (\Exception $ex){
                $transaction->rollBack();
                return false;
            }

            $transaction->commit();

            return $this->render('tours-index', [
                'modelDates'=> [$tourDate],
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else {
            $tour = Tour::find()->andWhere(['id' => $id])->one();
            $tourPreview = TourPreview::find()->where(['id' => $tour->tour_preview_id])->one();
            $modelDates = [];


            $dates = Json::decode($tour->dates, true);

            foreach ($dates as $key_day => $date) {
                if (sizeof($date) > 1) {
                    foreach ($date as $key => $date_element) {

                        $tempObject = new TourDate();
                        $tempObject->day_of_week = $key_day;
                        $tempObject->time = $key;
                        $tempObject->price_child = $date_element['price_child'];
                        $tempObject->price_adult = $date_element['price_adult'];
                        $modelDates[] = $tempObject;
                    }
                } else {
                    $tempObject = new TourDate();
                    $tempObject->day_of_week = $key_day;
                    $time = array_keys($date)[0];
                    $tempObject->time = $time;
                    $tempObject->price_child = $date[$time]['price_child'];
                    $tempObject->price_adult = $date[$time]['price_adult'];
                    $modelDates[] = $tempObject;
                }
            }


            return $this->render('tour-edit', [
                'tour' => $tour,
                'tourPreview' => $tourPreview,
                'modelDates' => $modelDates
            ]);

        }
    }

    public function actionCreateTour(){
        $tour = new Tour();
        $tourPreview = new TourPreview();
        $searchModel = new TourSearch();
        $tourDate = new TourDate();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($tour->load(Yii::$app->request->post()) &&  $tourPreview->load(Yii::$app->request->post())){

            $transaction = \Yii::$app->db->beginTransaction();
            try {


                if (!$tourPreview->save()) {
                    $transaction->rollBack();
                    return $this->render('tour-create', [
                        'modelDates'=> [$tourDate],
                        'tour' => $tour,
                        'tourPreview' => $tourPreview
                    ]);


                }
                $tour->tour_preview_id = $tourPreview->id;
                $tour->name = $tourPreview->name;

                $dates = Yii::$app->request->post('TourDate');

                $dateObject = new \stdClass();

                foreach ($dates as $date) {

                    $dayofWeek = $date['day_of_week'];
                    if(isset($dateObject->$dayofWeek)) {
                        $dateObject->$dayofWeek[Carbon::createFromFormat('H:i',$date['time'])->format('G:i')] =
                            [
                                'price_child'=>intval($date['price_child']),
                                'price_adult'=>intval($date['price_adult'])
                            ];
                    }
                    else {
                        $dateObject->$dayofWeek =
                            [
                                (Carbon::createFromFormat('H:i',$date['time'])->format('G:i'))=>[
                                    'price_child'=>intval($date['price_child']),
                                    'price_adult'=>intval($date['price_adult'])
                                ],
                            ];
                    }

                }

                $tour->dates = json_encode($dateObject);


                if (!$tour->save()) {

                    $transaction->rollBack();

                    return $this->render('tour-create', [
                        'modelDates'=> [$tourDate],
                        'tour' => $tour,
                        'tourPreview' => $tourPreview
                    ]);
                }
            }
            catch (\Exception $ex){
                $transaction->rollBack();
                var_dump($ex);
                return false;
            }

            $transaction->commit();

            return $this->redirect('tours');
        }

        return $this->render('tour-create', [
            'modelDates'=> [$tourDate],
            'tour' => $tour,
            'tourPreview' => $tourPreview
        ]);
    }

    public function actionRegister(){
        $userRegister = new SignupForm();

        if (Yii::$app->request->isAjax && $userRegister->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($userRegister);
        }

        if (Yii::$app->request->post()){
            if($userRegister->load(Yii::$app->request->post()) && $userRegister->validate()){
                $userRegister->signup();

                $user = User::find()->where(['username'=>$userRegister->username])->one();

                Yii::$app->user->login($user,  3600 * 24 * 30);
                return $this->render('index',[]);
            }
            else {
                var_dump('<pre>',$userRegister->getErrors(),'</pre>');die();
            }
        }
            return $this->render('user-create', [

                'model'=>$userRegister
            ]);

    }

    public function actionSubregion(){
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = self::getSubregions($cat_id);

                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionDelete($id = NULL){
        if($id == null) throw new HttpException('500','Страница не найдена');

        $model = Tour::find()->andWhere(['id'=>$id])->one();
        $tour_preview = TourPreview::find()->where(['id'=>$model->tour_preview_id])->one();

        if($tour_preview!=null) {
            $tour_preview->delete();
            $model->delete();
        } else {
            throw new HttpException('500','Страница не найдена');
        }
        return $this->redirect(['/']);  
    }

    public static function getSubregions($region){
        $czech = explode(PHP_EOL,
            'Прагу
Карловы Вары
Замок Добржиш
Кутну Гору
Крушовице
Замок Чешский Штернберг
Карлштейн
Замок Кршивоклат
Конопиште
Пивоваренный завод Kozel
замок Орлик
Чешский рай
Замок Сихров
Чешский Крумлов
северную Чехию
Замок Глубока
Мельник
Терезин
Детенице
Замок Жлебы
Замок Леднице
Тельч и Тршебич
Йиндрихув Градец
Моравский Крас');



        $europe = explode(PHP_EOL,
            'Дрезден
Саксонскую Швейцарию
Мейсен
Вену
Нюрнберг
Регенсбург
Мюнхен
Берлин
Бамберг
Пассау
Зальцбург
Париж
Замки Баварии
Швейцарию
Будапешт
Инсбрук
Венецию
Женеву
Бенилюкс
Милан
Фландрию');


        $czech_formatted = [];
        $europe_formatted =[];


        foreach ($czech as $el){
            $czech_formatted[] = ['id'=>$el,'name'=> $el];
        }

        foreach ($europe as $el){
            $europe_formatted[] = ['id'=>$el,'name'=> $el];
        }

        if($region === "По Чехии") return $czech_formatted;
        else return $europe_formatted;

    }
}
