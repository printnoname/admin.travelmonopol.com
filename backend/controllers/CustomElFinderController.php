<?php
/**
 * Created by PhpStorm.
 * User: WebMaster
 * Date: 20.12.2017
 * Time: 15:19
 */

namespace backend\controllers;

use mihaildev\elfinder\Controller;
use mihaildev\elfinder\volume\Local;
use Yii;
use yii\helpers\ArrayHelper;

class CustomElFinderController extends Controller {

    public $roots = [];
    public $disabledCommands = ['netmount'];
    public $watermark;

    private $_options;

    public function getOptions()
    {

        if($this->_options !== null)
            return $this->_options;

        $this->_options['roots'] = [];

        foreach($this->roots as $root){
            if(is_string($root))
                $root = ['path' => $root];

            if(!isset($root['class']))
                $root['class'] = Local::className();

            $root = Yii::createObject($root);

            /** @var \mihaildev\elfinder\volume\Local $root*/

            if($root->isAvailable()) {
                $root = $root->getRoot();
                $root['uploadAllow'] = ['image/png','image/jpeg','image/bmp','image/gif','image/x-ms-bmp'];
                $root['uploadDeny'] = [];
                $root['uploadOrder'] = ['allow','deny'];

                $user_directory = $root['path'] . '/' . \Yii::$app->user->id;
                $root['URL'] = $root['URL'] . '/' . \Yii::$app->user->id;

                if(!is_dir($user_directory)){
                    mkdir($user_directory);
                }

                $root['path'] = $user_directory;

                $this->_options['roots'][] = $root;
            }
        }

        if(!empty($this->watermark)){
            $this->_options['bind']['upload.presave'] = 'Plugin.Watermark.onUpLoadPreSave';

            if(is_string($this->watermark)){
                $watermark = [
                    'source' => $this->watermark
                ];
            }else{
                $watermark = $this->watermark;
            }

            $this->_options['plugin']['Watermark'] = $watermark;
        }

        $this->_options = ArrayHelper::merge($this->_options, $this->connectOptions);

        return $this->_options;
    }
}