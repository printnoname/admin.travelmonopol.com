<?php

namespace backend\controllers;

use Yii;
use backend\models\SignupForm;
use backend\models\UpdateForm;
use common\models\User;
use backend\models\UserSearch;
use backend\models\ChangePasswordForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superPermission'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
           if(($model = $model->signup())== NULL) throw new HttpException('500', 'Невозможно создать пользователя');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id=NULL)
    {
        if($id == NULL) throw new HttpException('404', 'Страница не найдена');

        $model = new UpdateForm();

        $model->user = $this->findModel($id);
        $model->username = $model->user->username;
        $model->email = $model->user->email;

        if (($model->load(Yii::$app->request->post()))!=NULL) {
            if(!$model->update()) throw new HttpException('500', 'Невозможно изменить пользовательские данные');
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword($id=NULL)
    {
        if($id == NULL) throw new HttpException('404', 'Страница не найдена');

        $model = new ChangePasswordForm();

        $model->user = $this->findModel($id);

        if (($model->load(Yii::$app->request->post()))!=NULL) {

            if(!$model->changePassword()) throw new HttpException('500', 'Невозможно изменить пользовательские данные');
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id=NULL)
    {
        if($id==NULL) throw new HttpException('404', 'Страница не найдена');
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax)
            return $this->renderList();
        else
            return $this->redirect(['index']);
    }

    public function actionBlock($id=NULL){
        if($id==NULL) throw new HttpException('404', 'Страница не найдена');
        $this->findModel($id)->block();

        if (Yii::$app->request->isAjax)
            return $this->renderList();
        else
            return $this->redirect(['index']);
    }

    public function actionUnblock($id=NULL){
        if($id==NULL) throw new HttpException('404', 'Страница не найдена');
        $this->findModel($id)->unblock();

        if (Yii::$app->request->isAjax)
            return $this->renderList();
        else
            return $this->redirect(['index']);
    }

    protected function renderList()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->sort->route = Url::toRoute(['index']);

        $method = Yii::$app->request->isAjax ? 'renderAjax' : 'render';

        return $this->$method('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChatEdit($id=null)
    {

        if($id==NULL) throw new HttpException('404', 'Страница не найдена');

        $model = $this->findModel($id);

        if (Yii::$app->request->post() !=NULL) {
            $chat_token = Yii::$app->request->post()["User"]["chat_token"];
            if($chat_token!=null) {
                $model->chat_token = $chat_token;
                if($model->update()) {
                    $searchModel = new UserSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->redirect('/user');
                }
            }
        }

        return $this->render('chat-edit', [
            'model' => $model,
        ]);
    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = NULL)
    {

        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
