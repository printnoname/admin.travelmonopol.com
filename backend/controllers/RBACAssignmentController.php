<?php
/**
 * Created by PhpStorm.
 * User: WebMaster
 * Date: 27.12.2017
 * Time: 17:37
 */

namespace backend\controllers;


use mdm\admin\controllers\AssignmentController;
use Yii;
use mdm\admin\models\Assignment;
use mdm\admin\models\searchs\Assignment as AssignmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class RBACAssignmentController extends AssignmentController{

    public function actionAssign($id=null) {

        if($id == NULL)
            throw new HttpException(404, 'Страница не найдена');

        $items = Yii::$app->getRequest()->post('items', []);
        $model = new Assignment($id);
        $success = $model->assign($items);
        Yii::$app->getResponse()->format = 'json';
        $this->redirect(['view','id'=>$id]);
        return array_merge($model->getItems(), ['success' => $success]);
    }

    /**
     * Assign items
     * @param string $id
     * @return array
     */
    public function actionRevoke($id=null)
    {
        if($id == NULL)
            throw new HttpException(404, 'Страница не найдена');

        $items = Yii::$app->getRequest()->post('items', []);
        $model = new Assignment($id);
        $success = $model->revoke($items);
        Yii::$app->getResponse()->format = 'json';
        $this->redirect(['view','id'=>$id]);
        return array_merge($model->getItems(), ['success' => $success]);
    }

}
