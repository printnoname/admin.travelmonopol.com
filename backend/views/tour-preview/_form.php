<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TourPreview */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-preview-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tour_id')->textInput() ?>

    <?= $form->field($model, 'img_src')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_city_string')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tour_region')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
