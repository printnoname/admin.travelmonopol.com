<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourPreviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tour Previews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-preview-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tour Preview', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tour_id',
            'img_src',
            'name',
            'price',
            //'type',
            //'tour_type',
            //'tour_city',
            //'tour_city_string',
            //'tour_region',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
