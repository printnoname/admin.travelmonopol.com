<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TourPreview */

$this->title = 'Create Tour Preview';
$this->params['breadcrumbs'][] = ['label' => 'Tour Previews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-preview-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
