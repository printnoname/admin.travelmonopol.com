<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



$this->title = \Yii::t('backend/forms','Change Chat Token');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('backend/forms','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('backend/forms','Change Chat Token');
?>
<div class="user-update">

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="site-signup">

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'chat_token')->textInput(['autofocus' => true])->label(\Yii::t('backend/forms','Chat Token')) ?>

                <div class="form-group">
                    <?= Html::submitButton(\Yii::t('backend/forms','Create') , ['class' =>  'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>