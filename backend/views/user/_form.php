<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="site-signup">

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(\Yii::t('common/models/user','Username')) ?>

                <?= $form->field($model, 'name')->label(\Yii::t('common/models/user','Name')) ?>
                <?= $form->field($model, 'surname')->label(\Yii::t('common/models/user','Surname')) ?>
                <?= $form->field($model, 'phone')->label(\Yii::t('common/models/user','Phone')) ?>

                <?= $form->field($model, 'email')->label(\Yii::t('common/models/user','Email'))?>

                <?= $form->field($model, 'password')->passwordInput()->label(\Yii::t('common/models/user','Password')) ?>

                <?= $form->field($model,'role')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\Yii::$app->authManager->getRoles(),'name','name'),
                        ['prompt'=>\Yii::t('common/models/user','Select Role')])
                    ->label(\Yii::t('common/models/user','Select Role')) ?>;

                <div class="form-group">
                    <?= Html::submitButton(\Yii::t('backend/forms','Create') , ['class' =>  'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
