<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = \Yii::t('backend/forms','Change Password');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('backend/forms','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->username, 'url' => ['view', 'id' => $model->user->id]];
$this->params['breadcrumbs'][] = \Yii::t('backend/forms','Update');
?>
<div class="user-update">


    <?= $this->render('change_password_form', [
        'model' => $model,
    ]) ?>

</div>
