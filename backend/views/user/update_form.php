<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="site-signup">

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'form-update']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(\Yii::t('common/models/user','Username')) ?>

                <?= $form->field($model, 'email')->label(\Yii::t('common/models/user','Email'))?>

                <div class="form-group">
                    <?= Html::submitButton(\Yii::t('backend/forms','Update') , ['class' =>  'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
