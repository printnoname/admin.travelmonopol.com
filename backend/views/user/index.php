<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use Carbon\Carbon;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('backend/forms','Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(\Yii::t('backend/forms','Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            //'auth_key',
           // 'password_hash',
           // 'password_reset_token',
            'email:email',
            //'blocked_at',
            //'status',
            ['attribute' => 'created_at',
                'filter' => DatePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'type' => DatePicker::TYPE_INPUT,
                        'options' => ['placeholder' => 'Выберите дату ...'],
                        'convertFormat' => false,
                        'pluginOptions' => [
                            'format' => 'dd-m-yyyy',
                            'autoclose'=> true,
                            'todayHighlight' => true
                        ]
                    ]),
                'value' => function($model){
                    return Carbon::createFromTimestamp($model->created_at)->toDateTimeString();
                }
            ],
            ['attribute'=>'status',
                'value'=>'status',
                'filter'=>Html::activeDropDownList($searchModel,
                    'status',
                    [10=>\Yii::t('backend/forms','Active'),9=>\Yii::t('backend/forms','Blocked')],
                    ['class'=>'form-control','prompt' => \Yii::t('backend/forms','Select Status')]),
                'value' => function($model){
                    if($model->status == 9) return \Yii::t('backend/forms','Blocked');
                    else return \Yii::t('backend/forms','Active');
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}{change-password} {change-rbac} {change-chat-token}',
                'contentOptions' => ['class' => 'action-column'],
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('backend/forms', 'Delete'),
                            'data-pjax' => '#model-grid',
                        ]);
                    },
                    'change-chat-token' => function ($url, $model, $key) {
                        $url = '/user/chat-edit?id=' . $model->id;
                        return Html::a('<span class="fa fa-comments-o fa-lg"></span>', $url, [
                            'title' => Yii::t('backend/forms', 'Change Chat Token'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'change-password' => function ($url, $model, $key) {
                        return Html::a('<span class="fa fa-key fa-lg"></span>', $url, [
                            'title' => Yii::t('backend/forms', 'Change Password'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'change-rbac' => function ($url, $model, $key) {
                        $url = '/admin/assignment/view?id=' . $model->id;
                        return Html::a('<span class="fa fa-id-card-o fa-lg"></span>', $url, [
                            'title' => Yii::t('backend/forms', 'Manage RBAC'),
                            'data-pjax' => '0',
                        ]);
                    },
                    ''
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
