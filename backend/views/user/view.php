<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('backend/forms','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('backend/forms','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('backend/forms','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'phone',
            'name',
            'surname',
            'email:email',
            ['attribute'=>'status',
                'value'=>function($model){
                    return $model->status == 10 ? \Yii::t('backend/forms','Active') : \Yii::t('backend/forms','Blocked');
                }
            ],
            ['attribute'=>'created_at',
                'value'=>function($model){
                    return Carbon::createFromTimestamp($model->created_at)->toDateTimeString();
                }
                ],
        ],
    ]) ?>

</div>
