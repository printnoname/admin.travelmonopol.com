<?php

use yii\helpers\Html;

$this->title = 'Edit Tour';
$this->params['breadcrumbs'][] = ['label' => 'Tours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-edit">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('edit_form', [
        'modelDates'=>$modelDates,
        'tour' => $tour,
        'tourPreview' => $tourPreview
    ]) ?>

</div>