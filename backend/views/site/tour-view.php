<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $tour->name;
$this->params['breadcrumbs'][] = ['label' => 'Tours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $tour->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $tour->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $tour,
        'attributes' => [
            ['attribute'=>'name',
                'label'=>'Название'],
            ['attribute'=>'description',
                'format'=>'html',
                'label'=>'Содержание'],
            ['attribute'=>'img_src',
                'label'=>'Изображение',
                'format'=>'image'],
            ['attribute'=>'dates',
                'label'=>'Даты',
                'format'=>'html',
                'value'=>function($model){
                    $dates = json_decode($model->dates,true);
                    $dates_formatted = "";
                    foreach ($dates as $key=>$date){
                        $dates_formatted .= "<div> День: " . $key . "<ul>";
                        foreach ($date as $key=>$time){
                            $dates_formatted.= ". <li>Время: " . $key . ". Детский билет: " . $time['price_child'] . ". Билет взрослый: " . $time['price_adult'] . "</li>";
                        }
                        $dates_formatted .= "</ul></div>";
                    }
                    return $dates_formatted;
                }]
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $tour_preview,
        'attributes' => [
            ['attribute'=>'img_src',
                'label'=>'Превью',
                'format'=>'image'],
            ['attribute'=>'language',
                'label'=>'Язык экскурсии'
            ],
            ['attribute'=>'tour_region',
                'label'=>'Регион',],
            ['attribute'=>'tour_city_string',
                'label'=>'Место',],
        ],
    ]) ?>

</div>