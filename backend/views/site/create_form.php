<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use kartik\time\TimePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\TabularInput;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

?>

<div class="tour-form">

    <?php $form = ActiveForm::begin([
//        'enableAjaxValidation'      => true,
//        'enableClientValidation'    => false,
//        'validateOnChange'          => false,
//        'validateOnSubmit'          => true,
//        'validateOnBlur'            => false,
    ]);?>

    <?= $form->field($tour, 'tour_preview_id')->textInput()->hiddenInput()->label(false) ?>
    <?= $form->field($tourPreview, 'type')->textInput(['maxlength' => true])->hiddenInput(['value' => json_encode(['crawl'=>false,'site'=>'admin'])])->label(false) ?>
    <?= $form->field($tour, 'created_by')->textInput()->hiddenInput(['value'=>\Yii::$app->getUser()->id])->label(false)?>

    <?= $form->field($tourPreview, 'name')->textInput(['maxlength' => true])->label('Название экскурсии')  ?>

    <?= $form->field($tourPreview, 'language')->dropDownList(['ru'=>'На русском','en'=>'На английском','cz' =>'На чешском'],['prompt'=>'Язык экскурсии'])->label('Язык экскурсии') ?>


    <?= $form->field($tour, 'description')->widget(CKEditor::className(),[
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',
            [
                'preset' => 'full',
                'inline' => false
            ])
    ])->label('Описание');
    ?>

    <div class="panel-body">

    <?= $form->field($tourPreview, 'price')->textInput()->label('Цена в евро'); ?>

        <?= $form->field($tourPreview, 'tour_region')->dropDownList(['В Европу'=>'В Европу','По Чехии'=>'По Чехии'],['prompt'=>'Выберите страну...'])->label('Страна') ?>
    <?= $form->field($tourPreview, 'tour_city_string')->widget(DepDrop::classname(), [
        'pluginOptions'=>[
            'depends'=>[Html::getInputId($tourPreview, 'tour_region')], // the id for cat attribute
            'url'=>Url::to(['/site/subregion'])
        ],

            ])->label('Место'); ?>

        <?= TabularInput::widget([
            'models' => $modelDates,
            'attributeOptions' => [
                'enableAjaxValidation'      => false,
                'enableClientValidation'    => false,
                'validateOnChange'          => true,
                'validateOnSubmit'          => true,
                'validateOnBlur'            => false,
            ],
            'columns' => [
                [
                    'name'  => 'day_of_week',
                    'type'  => 'dropDownList',
                    'title' => 'День недели',
                    'defaultValue' => 'Понедельник',
                    'items' => [
                        'Понедельник' => 'Понедельник',
                        'Вторник' => 'Вторник',
                        'Среда' => 'Среда',
                        'Четверг'=>  'Четверг',
                        'Пятница'=>  'Пятница',
                        'Суббота'=>  'Суббота',
                        'Воскресенье'=>  'Воскресенье'
                    ]
                ],
                [
                    'name'  => 'time',
                    'type'  => TimePicker::className(),
                    'options'=>[
                        'pluginOptions'=>[
                        'showSeconds' => false,
                        'showMeridian' => false,
                        ]
                    ]
                ],
                [
                    'name'  => 'price_child',
                    'options'=>[
                            'placeholder'=>'Цена за ребёнка',
                            'type'=>'number'
                    ]
                ],
                [
                    'name'  => 'price_adult',
                    'options'=>[
                        'placeholder'=>'Цена за взрослого',
                        'type'=>'number'
                    ]
                ],
            ]
        ]) ?>


    <?= $form->field($tourPreview, 'img_src')->widget(InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinderThumbnail', // вставляем название контроллера, по умолчанию равен elfinder
        'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ])->label('Превью');
    ?>

    <?= $form->field($tour, 'img_src')->widget(InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ])->label('Основное изображение');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
