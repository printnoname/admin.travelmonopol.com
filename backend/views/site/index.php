<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Главная';

?>
<div>
    <?php if(!(\Yii::$app->user->can('guidePermission') || \Yii::$app->user->can('superPermission')) ) {
       echo "<h3>Ваша заявка рассматривается администрацией сайта TravelMonopol.com.</h3>";
    }?>
</div>