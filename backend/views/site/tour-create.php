<?php

use yii\helpers\Html;

$this->title = 'Create Tour';
$this->params['breadcrumbs'][] = ['label' => 'Tours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('create_form', [
            'modelDates'=>$modelDates,
        'tour' => $tour,
        'tourPreview' => $tourPreview
    ]) ?>

</div>
