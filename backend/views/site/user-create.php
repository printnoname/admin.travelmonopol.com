<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = \Yii::t('backend/forms','Create User');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('backend/forms','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <?= $this->render('user-create-form', [
        'model' => $model,
    ]) ?>

</div>
