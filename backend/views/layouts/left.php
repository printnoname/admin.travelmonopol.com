<?php if(!\Yii::$app->user->isGuest) {
    ?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
<?php
            echo '<div class="user-panel" >
            <div class="pull-left image" >
                <img src="/img/user-pic160x160.png" class="img-circle" alt = "User Image" />
            </div >
            <div class="pull-left info" >
                <p>'  . common\models\User::find()->where(['id'=>\Yii::$app->getUser()->id])->one()->username . '</p >

                <a href = "#" ><i class="fa fa-circle text-success" ></i > Онлайн</a >
            </div >
        </div >';
        ?>
        <!-- search form -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree',],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Профиль','icon' => 'dashboard', 'url' => ['#'],'visible'=>!(\Yii::$app->user->isGuest)],
                    ['label' => 'Туры','icon' => 'dashboard', 'url' => ['/tours'],'visible'=>(\Yii::$app->user->can('superPermission') || \Yii::$app->user->can('guidePermission'))],
                    ['label' => 'Заказы','icon' => 'dashboard', 'url' => ['/orders'],'visible'=>(\Yii::$app->user->can('superPermission') || \Yii::$app->user->can('guidePermission'))],
                    ['label' => 'Управление гидами','icon' => 'dashboard', 'url' => ['/user'],'visible'=>\Yii::$app->user->can('superPermission')],
                    ['label' => 'Вход', 'url' => ['site/login'], 'visible' => \Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
<?php }?>