<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">TM</span><span class="logo-lg">TravelMonopol</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
         <?php if(!\Yii::$app->user->isGuest) { ?>
            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less--

                <!-- Tasks: style can be found in dropdown.less -->

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/img/user-pic160x160.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?=common\models\User::find()->where(['id'=>\Yii::$app->getUser()->id])->one()->username?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/img/user-pic160x160.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?=common\models\User::find()->where(['id'=>\Yii::$app->getUser()->id])->one()->username?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="col-xs-12 text-center">
                                <?= Html::a(
                                    'Выход',
                                    ['/site/logout'],
                                    ['data-method' => 'post','style'=>'width:100%', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
            </ul>
                <?php } ?>
        </div>
    </nav>
</header>
