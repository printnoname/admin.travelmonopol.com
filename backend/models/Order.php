<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tours".
 *
 * @property int $id
 * @property int $tour_preview_id
 * @property integer $user_id
 * @property string $data
 *
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_preview_id', 'user_id', 'data'], 'required'],
            [['tour_preview_id','user_id'], 'integer'],
            [[ 'data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_preview_id' => 'Tour Preview ID',
            'user_id' => 'User ID',
            'data' => 'Data'
        ];
    }

    public static function find()
    {
        return parent::find()->where(['user_id'=>\Yii::$app->getUser()->id]);
    }
}
