<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tour_previews".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $img_src
 * @property string $name
 * @property int $price
 * @property string $type
 * @property string $tour_type
 * @property string $tour_city
 * @property string $tour_city_string
 * @property string $tour_region
 * @property string $language
 */
class TourPreview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour_previews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img_src', 'name', 'price', 'type','language'], 'required','on'=>['insert','update']],
            [['tour_id', 'price'], 'integer'],
            [['img_src', 'name', 'price','language'], 'required'],
            [['img_src'], 'string', 'max' => 2083],
            [['name', 'tour_type', 'tour_city', 'tour_city_string', 'tour_region','language'], 'string', 'max' => 191],
            [['type'], 'string', 'max' => 2000],
            [['tour_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tour ID',
            'img_src' => 'Img Src',
            'name' => 'Name',
            'price' => 'Price',
            'type' => 'Type',
            'tour_type' => 'Tour Type',
            'tour_city' => 'Tour City',
            'tour_city_string' => 'Tour City String',
            'tour_region' => 'Tour Region',
            'language'=> 'Language'
        ];
    }
}
