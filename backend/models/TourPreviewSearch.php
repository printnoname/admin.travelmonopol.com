<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TourPreview;

/**
 * TourPreviewSearch represents the model behind the search form of `backend\models\TourPreview`.
 */
class TourPreviewSearch extends TourPreview
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tour_id', 'price'], 'integer'],
            [['img_src', 'name', 'type', 'tour_type', 'tour_city', 'tour_city_string', 'tour_region'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TourPreview::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tour_id' => $this->tour_id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'img_src', $this->img_src])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'tour_type', $this->tour_type])
            ->andFilterWhere(['like', 'tour_city', $this->tour_city])
            ->andFilterWhere(['like', 'tour_city_string', $this->tour_city_string])
            ->andFilterWhere(['like', 'tour_region', $this->tour_region]);

        return $dataProvider;
    }
}
