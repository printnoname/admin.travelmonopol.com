<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 05.03.2018
 * Time: 1:52
 */

namespace backend\models;


use yii\base\Model;
use yii\db\ActiveRecord;

class TourDate extends Model
{
    public $day_of_week;
    public $time;
    public $price_child;
    public $price_adult;

    public function rules()
    {
        return [
            [['day_of_week','time','price_child','price_adult'],'required'],
            [['price_child','price_adult'],'integer'],
            [['time','day_of_week'],'string'],
            [['time','day_of_week'],'unique']
        ];
    }
    public function attributes()
    {
        return [
            'day_of_week','time','price_child','price_adult'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'day_of_week'=>'week day',
            'time' => 'time',
            'price_child' => 'Price for child',
            'price_adult'=>'Price for adult'
        ];
    }
}