<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class UpdateForm extends Model
{
    public $username;
    public $email;
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.',
                'on'=>'update','when'=>function($model){return $model->isAttributeChanged('username');}],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'message' => 'This email address has already been taken.',
                'on'=>'update','when'=>function($model){return $model->isAttributeChanged('email');}
               ],

        ];
    }

    public function update()
    {

        if (!$this->validate()) {
            return false;
        }

        $this->user->username = $this->username;
        $this->user->email = $this->email;

        if(!$this->user->update()) return false;
        return true;

    }
}
