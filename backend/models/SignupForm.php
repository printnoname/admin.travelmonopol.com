<?php
namespace backend\models;

use backend\utilities\AdminMailer;
use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;
    public $surname;
    public $role;
    public $id;
    public $phone;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Пользователь с таким именемм уже существует'],
            ['username', 'string', 'min' => 5, 'max' => 255],

            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['surname', 'trim'],
            ['surname', 'required'],
            ['surname', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'trim'],
            ['phone', 'required'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Пользовательн с таким email\'ом уже существует'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['role','string']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            var_dump($this->getErrors(),'here1');
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();

            $user->username = $this->username;
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->phone = $this->phone;

            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailActivationToken();
            $user->status  = 9;



            if(!$user->save()){
                var_dump($user->getErrors(),'here1');
                throw new \Exception('save problem');
            }

            if ($this->role != null) {
                $authManager = Yii::$app->getAuthManager();
                if ($authManager->assign($authManager->getRole($this->role),$user->id) == NULL) throw new \Exception('role problem');
            }

            $data = ["activation_link"=>'http://travelmonopol.com/activate_user?email=' . $user->email . '&token=' . $user->email_activation_token,"email"=>$user->email];
            $emailer = new AdminMailer();
            $emailer->sendTourFormForClient($data);

        } catch (\Exception $ex) {
            var_dump($ex->getMessage(),'problem_here');
            $transaction->rollBack();
            return null;
        }

        $transaction->commit();

        return $user;

    }
}
