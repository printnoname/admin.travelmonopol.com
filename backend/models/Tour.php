<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tours".
 *
 * @property int $id
 * @property int $tour_preview_id
 * @property string $img_src
 * @property string $name
 * @property string $description
 * @property string $dates
 * @property integer created_bys
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_preview_id', 'img_src', 'name', 'description', 'dates','created_by'], 'required','on'=>'insert'],
            [[ 'img_src', 'name', 'description'], 'required'],
            [['tour_preview_id','created_by'], 'integer'],
            [['description', 'dates'], 'string'],
            [['img_src'], 'string', 'max' => 2083],
            [['name'], 'string', 'max' => 191],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_preview_id' => 'Tour Preview ID',
            'img_src' => 'Img Src',
            'name' => 'Name',
            'description' => 'Description',
            'dates' => 'Dates',
        ];
    }

    public static function find()
    {
        $tour = parent::find()->where(['created_by'=>\Yii::$app->getUser()->id]);
        if($tour->count()>0){
            $preview = TourPreview::find()->where(['id'=>$tour->one()->tour_preview_id]);
            if($preview->count()>0){
                return $tour;
            }
        }
     return parent::find()->where(['id'=>0]);
    }
}
