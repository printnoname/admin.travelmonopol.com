<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class ChangePasswordForm extends Model
{

    public $password;
    public $user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

        ];
    }

    public function changePassword()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->user->setPassword($this->password);

        if(!$this->user->update()) return false;
        return true;

    }
}
