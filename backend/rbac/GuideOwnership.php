<?php

/**
 * Created by PhpStorm.
 * User: noname
 * Date: 14.04.2018
 * Time: 15:33
 */
namespace backend\rbac;

use yii\rbac\Rule;
use common\models\User;

class GuideOwnership extends Rule
{
    public $name = 'isOwner';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['post']) ? $params['post']->user == $user : false;
    }
}
