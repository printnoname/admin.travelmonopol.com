<?php

namespace backend\utilities;

use PHPMailer\PHPMailer\PHPMailer;

class AdminMailer
{
    public $guideMailer;


    public function __construct()
    {
        $this->guideMailer = new PHPMailer();
    }


    public function sendTourFormForClient($data){
        $this->setupMailerForGuide($data);
        $this->guideMailer->send();
    }

    private function setupMailerForGuide($data){

        $this->guideMailer->isSMTP();

        $this->guideMailer->Host = "email.active24.com";

        $this->guideMailer->SMTPAuth = true;

        $this->guideMailer->Username = "info@travelmonopol.com";
        $this->guideMailer->Password = "Upov00000";

        $this->guideMailer->SMTPSecure = "tls";

        $this->guideMailer->Port = 587;

        $this->guideMailer->From = "info@travelmonopol.com";
        $this->guideMailer->FromName = 'TravelMonopol';
        $this->guideMailer->CharSet = 'UTF-8';

        $this->guideMailer->addReplyTo("info@travelmonopol.com", "Reply");

        $this->guideMailer->addAddress($data["email"]);

        $this->guideMailer->isHTML(true);

        $this->guideMailer->Subject  = "Регистрация на TravelMonopol";

        $this->guideMailer->Body= "<p>Уважаемый/ая</p>
        <p>Вы успешно зарегистрировались на сайте travelmonopol.com</p>
        <p>Теперь необходимо активировать Вашу учетную запись. Для активации - перейдите по этой ссылке: </p>
        <p>" . $data["activation_link"] . "</p>
        <p>Если Вы не отправляли запрос, просто проигнорируйте данное письмо.</p>
        ";
    }
}