<?php
/**
 * Created by PhpStorm.
 * User: WebMaster
 * Date: 21.12.2017
 * Time: 12:09
 */

namespace backend\utilities;

use mihaildev\elfinder\PluginInterface;
use Imagick;

class ImageManager extends PluginInterface
{

    public $bind = [
        'upload.pre'=>'checkResolution',
        'mkdir.pre'=>'checkValidLanguage',
        'upload.presave' => ['resizeCrop','changeName'],
        'rm.pre rename.pre '=>'deleteImage',
        'copy.pre paste.pre'=>'moveImage'

    ];

    public $path_to_folder;
    /**
     * @return string
     */
    public function getName()
    {
        return 'ImageManager';
    }

    public function deleteImage($cmd, &$args, $elfinder, $volume) {
    return array(
                    'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
                    'results' => array(
                        'error' => 'Переименование и удаление изображения запрещено',
                        'sync' => true         // true - refresh elFinder view
                    )
                );
    }

    public function moveImage($cmd, &$args, $elfinder, $volume) {
        return array(
            'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
            'results' => array(
                'error' => 'Перемещение и копирование изображений запрещено',
                'sync' => true         // true - refresh elFinder view
            )
        );
    }
    public function checkResolution($cmd, &$args, $elfinder, $volume){

        $src = $args['FILES']['upload']['tmp_name'][0];
        $name =  $args['FILES']['upload']['name'][0];

        $ext = pathinfo($name, PATHINFO_EXTENSION);


            $blob = file_get_contents($src);

            if($blob!=null) {
                $image = new Imagick();
                $image->readImageBlob($blob);

                $width = $image->getImageWidth();
                $height = $image->getImageHeight();

                if ($width < $this->getOption('width', $volume) || $height < $this->getOption('height', $volume)) {
                    return array(
                        'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
                        'results' => array(
                            'error' => 'Изображение не подходит под минимальное разрешение. Минимальная высота в пикселях - ' .
                                $this->getOption('height', $volume) .
                                ' . Минимальная ширина в пикселях - ' . $this->getOption('width', $volume),
                            'sync' => true         // true - refresh elFinder view
                        )
                    );
                }

                return true;

            } else {
                return array(
                    'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
                    'results' => array(
                        'error' => 'Ошибка при загрузке изображения',
                        'sync' => true         // true - refresh elFinder view
                    )
                );
            }
    }

    public function resizeCrop(&$path, &$name, $src, $elfinder, $volume){

        if(!$this->isEnable($volume)) {
            return array(
                'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
                'results' => array(
                    'error' => 'Ошибка при загрузке изображения',
                    'sync' => true         // true - refresh elFinder view
                )
            );
        }

        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $filename = pathinfo($name, PATHINFO_FILENAME);

           $blob = file_get_contents($src);

           if($blob!=null) {
               $image = new Imagick();

               $image->readImageBlob($blob);
               $image->setImageBackgroundColor('transparent');
               $width = $image->getImageWidth();
               $height = $image->getImageHeight();

               if($this->getOption('operation_flag', $volume) === 0) $this->cropToThumbnail($image,$width,$height,$src,$volume,$ext);
               else $this->cropToNormal($image,$width,$height,$src,$volume,$ext);

               return true;

           } else {
               return array(

                   'preventexec' => true,
                   'results' => array(
                       'error' => 'Произошла ошибка при загрузке изображения',
                       'sync' => true
                   )
               );
           }
    }

    private function cropToThumbnail($image,$width,$height,$src,$volume,$ext){
        try {
            if ($ext == 'gif') {
                $image = $image->coalesceImages();
                foreach ($image as $image_frame) {
                    $image_frame->cropThumbnailImage($this->getOption('width', $volume), $this->getOption('height', $volume));
                }
                $image->writeImages($src, true);

            } else {
                $image->cropThumbnailImage($this->getOption('width', $volume), $this->getOption('height', $volume));
                $image->writeImage($src);
            }
        } catch (\Exception $ex){
            return array(

                'preventexec' => true,
                'results' => array(
                    'error' => 'Произошла ошибка при обработке изображения',
                    'sync' => true
                )
            );
        }
    }

    private function cropToNormal($image,$width,$height,$src,$volume,$ext){

        try {
            $width_changed = 1;
            $height_changed = 1;

            if ($ext == 'gif') {
                $image = $image->coalesceImages();
                foreach ($image as $image_frame) {
                    if ($width > 1024 || $height > 1024) {
                        if ($width > $height) {
                            $assoc = $width / $height;
                            $width_changed = 768;
                            $height_changed = $width_changed * $assoc;
                            $image_frame->resizeImage($width_changed, $height_changed, Imagick::FILTER_CUBIC, 0.8);
                            $image_frame->setImagePage(0, 0, 0, 0);
                        } else {
                            $assoc = $height / $width;
                            $height = 768;
                            $width_changed = $height_changed * $assoc;
                            $image_frame->resizeImage($width_changed, $height_changed, Imagick::FILTER_CUBIC, 0.8);
                            $image_frame->setImagePage(0, 0, 0, 0);
                        }
                    }

                }
                $image->writeImages($src, true);

            } else {
                if ($width > 1024 || $height > 1024) {
                    if ($width > $height) {
                        $assoc = $height / $width;
                        $width_changed = 768;
                        $height_changed = $width_changed * $assoc;
                        $image->resizeImage($width_changed, $height_changed, Imagick::FILTER_CUBIC, 0.5);
                    } else {
                        $assoc = $width / $height;
                        $height_changed = 768;
                        $width_changed = $height_changed * $assoc;
                        $image->resizeImage($width_changed, $height_changed, Imagick::FILTER_CUBIC, 0.5);
                    }

                    $image->writeImage($src);
                }
            }
        } catch (\Exception $ex){
            return array(

                'preventexec' => true,
                'results' => array(
                    'error' => 'Произошла ошибка при обработке изображения',
                    'sync' => true
                )
            );
        }
    }



    public function changeName(&$path, &$name, $src, $elfinder, $volume) {
        if(!$this->isEnable($volume)) {
            return false;
        }

        $ext = pathinfo($name, PATHINFO_EXTENSION);

        $name = hash('md5', $name . strval(time())) . '.' . $ext ;
    }

    public function checkValidLanguage($cmd, &$args, $elfinder, $volume)
    {

        $name = $args['name'];

        for ($i = 0; $i < strlen($name); $i++) {
            if (ord($name[$i]) > 127) return array(
                'preventexec' => true, // prevent do exec and discard the callback that is registered to it after
                'results' => array(
                    'error' => "Недопустимое имя для папки. В названии присутствуют кирилические символы",
                    'sync' => true         // true - refresh elFinder view
                )
            );
        }

        return true;
    }

}